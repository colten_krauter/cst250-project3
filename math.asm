#	t2 - Overflow Check
#	t3 - Overflow Bitmask
#	t4 - Parse Operator
#	t6 - Parse Result
#	a0 - Math Answer
#	a1 - Error Register
#	v0 - Current Operator
math:
li $t5, 2
beq $v0, $t5, sub #if V0 == 2 goto sub
nop
li $t5, 3
beq $v0, $t5, add #if V0 == 3 goto add
nop
li $t5, 4
beq $v0, $t5, mul #if V0 == 4 goto mul
nop

sub: #subtracts $t6 from $a0
subu $a0, $a0, $t6
j check
nop

add: #adds $a0 with $t6
addu $a0, $a0, $t6
j check
nop

mul: #Multiplies $a0 with $t6
mullo $a0, $a0, $t6
j check
nop

check: #performs Overflow check
	li $t3, 0x80000000	#Creates a bitmask to check sign of final result
	and $t2, $a0, $t3
	beq $t2, $t3 over
	nop
	#Secondary overflow code
	#branch over
	#nop
	jr $ra
	nop
over:
	li $a1, 4
	jr $ra
	nop