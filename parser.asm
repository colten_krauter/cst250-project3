# New ASM File
.org 0x10000000
#	t0 - Status Checking
#	t1 - ASCII Characters
#	t2 - Binary Values
#	t5 - UART Input
#	t3 - Negative Boolean
#	t4 - Parse Operator
#	t6 - Parse Result
#	a0 - Math Answer
#	a1 - Error Register
#	v0 - Current Operator
Begin_Parse:
li $s0, 0xf0000000 #Control
li $s1, 0xf0000004 #Status
li $s2, 0xf000000c #Send
li $s3, 0xf0000008 #Receive
li $t4, 0b00000001 #BitMask1
li $s5, 0b00000010 #BitMask2
li $t6, 10	         #10

polling:	#Waits for data from UART
	lw $t0, 0($s1)	
	and $t0, $t0, $s5	#Bitmask Check Ready Bit
	bne $t0, $s5, polling
	nop
#End polling

receive:	#Receives incoming UART data
	lw $t5, 0($s3)	#Loads Buffer Data
	sw $s5, 0($s0)	#Writes Clear Status
	j parse
	nop
#End receive

parse:	#Parses ASCII data into Decimal
	#Equals
	li $t1, 0x3D
	li $s4, 1
	beq $t5, $t1, out
	nop
	#Plus
	li $t1, 0x2B
	li $s4, 3
	beq $t5, $t1, out
	nop
	#Star
	li $t1, 0x2A
	li $s4, 4
	beq $t5, $t1, out
	nop
	#Minus
	li $t1, 0x2D
	beq $t5, $t1, minus
	nop
	#Zero
	li $t1, 0x30
	li $t2, 0
	beq $t5, $t1, next
	nop
	#One	
	li $t1,  0x31
	li $t2, 1
	beq $t5, $t1, next
	nop	
	#Two
	li $t1,  0x32
	li $t2, 2
	beq $t5, $t1, next
	nop
	#Three
	li $t1,  0x33
	li $t2, 3
	beq $t5, $t1, next
	nop
	#Four
	li $t1,  0x34
	li $t2, 4
	beq $t5, $t1, next
	nop
	#Five
	li $t1,  0x35
	li $t2, 5
	beq $t5, $t1, next
	nop
	#Six
	li $t1,  0x36
	li $t2, 6
	beq $t5, $t1, next
	nop
	#Seven
	li $t1,  0x37
	li $t2, 7
	beq $t5, $t1, next
	nop
	#Eight
	li $t1,  0x38
	li $t2, 8	
	beq $t5, $t1, next
	nop
	#Nine
	li $t1,  0x39
	li $t2, 9
	beq $t5, $t1, next
	nop
	#Space
	li $t1, 0x20		#Program ignores space completely.
	beq $t5, $t1, receive	#Allows for weird inputs like "1  2 +     6="
				#This would be inperpreted as "12+6="
	#Other
	li $t5, 0x31
	li $s6, 0
	li $a1, 1
	j error
	nop
#End parse

next:
mullo $s6, $s6, $t6 #multiply current result by 10
addu $s6, $s6, $t2  #adds new number to result
j receive
nop
#End next

minus:
beq $0, $0, m_operator #if previous character was number
beq $0, $0, m_negative #if previous character was operator and toggle is off or this is the first character
beq $0, $0, m_error #if previous character was operator and toggle is on
	m_operator:
	li $s4, 2	#Code 2: Subtraction
	j out
	nop
	m_negative:
	li $t3, 1	#toggle on
	j receive	#get next character
	nop
	m_error:
	li $a1, 2
	j error
#End minus

out:
bne $t3, $0, neg
nop
jr $ra
nop

neg:	#Converts result to it's two's complement
	nor $s6, $s6, $s6	#bitwise NOT
	addu $s6, $s6, $t4	#Plus 1
	li $t3, 0
	j out
	nop
error:
li $s6, 0
jr $ra
nop